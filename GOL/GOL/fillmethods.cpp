#include "header.hpp"
#include <time.h>

void RandomFill(bool* tab) {
	srand(time(NULL));      /* remplissage al�atoire : on utilise srand pour g�n�rer un nombre pseudo-al�atoire */
	for (int i = 0;i < N;i++) {     /* pour chaque case de la grille */
		tab[i] = (rand() % 2 == 1); /* on affecte la valeur bool�enne d'un test : 0 (cellule morte) si rand est pair, 1 (cellule vivante) si rand est impair */
	}
}

void ChooseFill(bool* tab) {
	for (int i = 0;i < N;i++) {
		char choice; /* remplissage manuel : on d�clare un caract�re 'choice' */
		printf("Data pour valeur %d (0=mort, sinon vivant. Veuillez n'ins�rer qu'un seul caract�re.)", i + 1);
		cin >> choice;
		tab[i] = choice; /* on demande � l'utilisateur de saisir la valeur de choice : 0 (morte) / autre (vivante) avant de l'affecter � une case du tableau */
	}
}

void DebugFill(bool* tab) {
	for (int i = 0;i < N;i++) {
		if (i < 3 || i>6 && i <= 9 || i >= 53 && i <= 55) tab[i] = true;
		else tab[i] = false; /* d�finition particuli�res des cellules vivantes et mortes d'une grille � des fins de v�rification */
	}
}
