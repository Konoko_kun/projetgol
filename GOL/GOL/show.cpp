#include "header.hpp"
#include <iomanip>

void ShowTable(bool* tab,int nIter) {
	cout << "Iteration n " << nIter << endl;  /* est affich� le num�ro de l'it�ration */
	for (int i = 0;i < S;i++) {
		for (int j = 0;j < S;j++) {
			cout << setw(2);                  /* on ins�re un espace de 2 entre chaque caract�re */
			if (tab[j + 10 * i]) cout << "*"; /* on v�rifie le statut de la cellule par rapport � la valeur de son index (0 � 99) : une cellule vivante (true) est repr�sent�e graphiquement par '*' */
			else cout << ".";                 /* tandis qu'une cellule morte correspond � '.' */
		}
		cout << endl;
	}
}
