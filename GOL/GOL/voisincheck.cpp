#include "header.hpp"

int VoisinCheck(bool* tab, int tested) {
	int nbVoisin = 0;
	if((tested / S !=0) && (tested % S !=0) && tab[tested-11])nbVoisin++;//H+G /* on teste le nombre de voisins vivants en haut � gauche (diagonale) */
	if((tested / S !=0) && tab[tested-10])nbVoisin++;//H
	if((tested / S !=0) && (tested % S !=9) && tab[tested-9]) nbVoisin++;//H+D
	if((tested % S !=0) && tab[tested-1]) nbVoisin++;//G    /* on teste le nombre de voisins vivants � gauche */
	if((tested % S !=9) && tab[tested+1]) nbVoisin++;//D
	if((tested / S !=9) && (tested % S !=0) && tab[tested+9]) nbVoisin++;//B+G
	if((tested / S !=9) && tab[tested+10])nbVoisin++;//B
	if((tested / S !=9) && (tested % S !=9) && tab[tested+11])nbVoisin++;//B+D
	return nbVoisin; /* la fonction renvoie le nombre de cellules voisines vivantes pour une cellule donnee */
}
