#include "header.hpp"

bool* BeginIteration(bool* tab1){
	bool* tableRes= new bool[N]; /* la grille est repr�sent�e informatiquement par un tableau de 100 bool�ens, on en g�n�re ici un nouveau dans le cadre d'une it�ration du jeu */
	for (int i = 0;i < N;i++) {
		int nbVois = VoisinCheck(tab1,i);               /* on recup�re le nombre de voisins vivants de chaque cellule d'index i via la fonction VoisinCheck */
		if (tab1[i]) tableRes[i]=SurvivalCheck(nbVois); /* on v�rifie si la cellule survit dans le cas o� elle est vivante */
		else tableRes[i]=BirthCheck(nbVois);            /* on d�termine si la cellule nait sachant qu'elle est morte */
	}
	return tableRes; /* renvoie la grille obtenue � l'issue d'une iteration */
}
