// GOL.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <stdlib.h>
#include "header.hpp"

using namespace std;

int main()
{
	bool* tabCurrent = new bool[N]; /* création d'une nouvelle grille */
	Init(tabCurrent); /* Initialisation du contenu de la grille */
	ShowTable(tabCurrent,0); /* affichage graphique du résultat */

	int iter = 1;
	char input;
	do {                                            /* faire */
		tabCurrent = BeginIteration(tabCurrent);    /* redéfinir le statut des cellules par rapport aux valeurs de départ */
		ShowTable(tabCurrent, iter);                /* représenter graphiquement l'état des cellules pour l'itération courante */
		AskContinuation(&input);                    /* demander à l'utilisateur s'il veut continuer */
		iter++;                                     /* augmenter le nombre d'itérations de 1 */
	} while (input=='y');                           /* tant que l'utilisateur entre 'y' (yes) */
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer :
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
