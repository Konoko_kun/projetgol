#include "header.hpp"
bool SurvivalCheck(int nbVoisin) {
	if (nbVoisin == 2 || nbVoisin == 3) return true;
	else return false; /* d'apr�s les r�gles du jeu de la vie, si une cellule vivante a 2 ou 3 cellules voisines vivantes elle survit (1), sinon (plus de 3 ou moins de 2) elle meurt (0)*/
}

bool BirthCheck(int nbVoisin) {
	if (nbVoisin == 3) return true; /* si une cellule morte a 3 cellules voisines vivantes elle devient vivante ("na�t"), sinon (strictement inf�rieur ou sup�rieur � 3) elle reste morte */
	else return false;
}
