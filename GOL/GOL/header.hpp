#include <iostream> /*Le fichier d'en-t�te : il contient toutes les biblioth�ques, constantes, et prototypes de fonction n�cessaires au projet*/
#include <stdlib.h>
#define S 10
#define N 100

using namespace std;

void tempo();
void Init(bool*);
void ShowTable(bool*,int);
int VoisinCheck(bool*, int);
bool SurvivalCheck(int);
bool BirthCheck(int);
bool* BeginIteration(bool*);
void AskContinuation(char*);
void RandomFill(bool*);
void ChooseFill(bool*);
void DebugFill(bool*);
