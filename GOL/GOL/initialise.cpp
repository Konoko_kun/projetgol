#include "header.hpp"

void Init(bool* tab) {

	bool f;
	do {
		char c;
		f = true;
		cout << "Mode random 'r' ou choix 'c' ?"; cin >> c; /* Demande � l'utilisateur s'il souhaite initialiser la grille al�atoirement ou non en saisissant r, respectivement c */
        switch (c) {                                        /*  tant qu'il n'a pas effectu� de saisie valide */
		case 'r': RandomFill(tab); break;
		case 'c': ChooseFill(tab); break;                   /* Le tableau est rempli par l'utilisateur dans ce cas-ci */
		case 'd': DebugFill(tab); break;
		default: f = false; break;                          /* Si l'utilisateur a saisi autre chose que ce qui est disponible, le bool�en f prend la valeur "false" */
		}
	} while (!f);                                           /* ... ce qui r�ex�cute la boucle */
}
